
#include "Arduino.h"
#include "otis.h"
#include "vcnl.h"
#include <Servo.h>
#include <ArduinoJson.h>

#define TRUE 1
#define FALSE 0

#define JAUNE 'j'
#define VERT 'v'


#define VISION_THRESHOLD_SEE_JAUNE 3300
#define VISION_THRESHOLD_SEE_VERT 3050

//========Hardware Limits========

//===Mechanics===
#define WINCHSPEED 256  //The pwm value for the winch's motors

//These are cmd values for servos (from 0 to 180).
#define L_SHAFT_DIST_SERVO_MIN 60        //60 is optimal
#define L_SHAFT_DIST_SERVO_MAX 120       //120 is optimal

#define U_SHAFT_DIST_SERVO_MIN 60        //60 is optimal
#define U_SHAFT_DIST_SERVO_MAX 120       //20 is optimal

#define BALL_TRAPDOOR_SERVO_MIN 0
#define BALL_TRAPDOOR_SERVO_MAX 180


#define DOOR_SERVO_MIN 3  //90            //ces angles correspondent à la course du servo depuis l'ouverture maximale
#define DOOR_SERVO_MAX 168//170  //150    //Fermeture max




//======== SOFTWARE LIMITS (related to subscribed channel) =============
#define CHAN_DEF_SHAFT_CMD_CLEAR 0      //value no effect (influence) on the shafts
#define CHAN_DEF_SHAFT_CMD_FALL_SPOT 255     //value when spots fall, => going to reset the nbspots

#define CHAN_DEF_TIM_GAME_RESET 255   //timer's value when reset

#define CHAN_DEF_VISION_CLEAR 0   //no effect on the vision
#define CHAN_DEF_VISION_BLIND 1   //robot is blind
#define CHAN_DEF_VISION_SEE   2   //robot is forced to see

#define CHAN_DEF_OUVERTURE_PORTE 0  //the door is not opened




//============Publishing channels================
#define NB_PUBLISHING_CH 1
#define SPOTS_NB_BALLES 80
//============Subscribing channels================
#define NB_SUBSCRIBING_CH 3

#define RPI_SPOT_PORTES 1
#define RPI_SPOT_SHAFT 2
#define RPI_SPOT_INHIB_VISION 3 //Enable or disable the automated behavior
#define MASTER_COLOR 32
#define MASTER_TIMER 33

//==========These defines are used for init of register only=========

#define INIT_FLAG_IDLE           FALSE      //will be changed after updating device finished
#define INIT_FLAG_ACK            FALSE
#define INIT_NSPOT               0
#define INIT_FLAG_CATCH_ATTEMPT  FALSE      //If attempt to catch a new spot
#define INIT_FLAG_LAST_CATCH     FALSE      //If last attempt worked
#define INIT_ACT_DOOR            0          //Precise
#define INIT_ACT_CLOSE_DOOR      FALSE
#define INIT_ACT_ASSIMIL         FALSE
#define INIT_ACT_CLOSE_ASSIMIL   FALSE
#define INIT_ACT_SMALL_ELEVATION FALSE
#define INIT_ACT_LIBERATE        FALSE


//Capteurs,
#define INIT_QRDSIR              0
#define INIT_VCNL                0


//Devices,servo, from
#define INIT_SLD_CMD             20
#define INIT_SRD_CMD             20

//from 0 to 255
#define INIT_SLS_CMD             100
#define INIT_SUS_CMD             100

//0 or 1 (on off)
#define INIT_SB_CMD              0
#define INIT_SCAM_CMD            0
//Devices, motors,
#define INIT_MOTU                0  //Upper motors off
#define INIT_MOTL                0  //Lower motors off
#define INIT_SLDCMDSELECTIVE     0  //default inactive  (0)
#define INIT_SRDCMDSELECTIVE     0  //Default inactive (0)




#define INIT_SUSCMDMAX           255
#define INIT_SLSCMDMAX           255
#define INIT_SLDCMDMAX           255
#define INIT_SRDCMDMAX           255


//SHARED WITH CHANNELS

#define INIT_CHAN_INHIB_VISION  CHAN_DEF_VISION_CLEAR  //this is going 
#define INIT_CHAN_TIMEGAME    CHAN_DEF_TIM_GAME_RESET  //start with a reset        
#define INIT_CHAN_COLOR       JAUNE               //start with jaune
#define INIT_CHAN_SHAFT       CHAN_DEF_SHAFT_CMD_CLEAR  //start normally          
//#define INIT_CHAN_DOORS       0 //this is nothing int the enumera






  //SHAFT 
  #define SHAFT_OPENED 255
  #define SHAFT_CLOSED 0
  #define SHAFT_UPPER_SPOT 105//110
  #define SHAFT_LOWER_SPOT 102//110

//==================================================================


//variable
volatile unsigned char otisReg[OTIS_RSIZE];
bool newExec = true;
//volatile uint8_t publishing_to_channels[NB_PUBLISHING_CH] = {SPOTS_NB_BALLES};


//Servos
Servo serv_sld;
Servo serv_srd;
Servo serv_slf;
Servo serv_suf;
Servo serv_sb;
Servo serv_cam;

//JSON objects
StaticJsonBuffer<200> jsonBuffer;
JsonObject& json_send = jsonBuffer.createObject();
JsonObject& json_rcv;

bool stringComplete = false;


//function
void otisInitRegisters(volatile unsigned char *registers)
{
  
  registers[flagIdle] = INIT_FLAG_IDLE;
  registers[flagAck] = INIT_FLAG_ACK;
  registers[nSpot] = INIT_NSPOT;
  registers[flagCatchAttempt] = INIT_FLAG_CATCH_ATTEMPT;
  registers[flagLastCatch] = INIT_FLAG_LAST_CATCH;
  registers[actDoor] = INIT_ACT_DOOR;
  registers[actCloseDoor] = INIT_ACT_CLOSE_DOOR;
  registers[actAssimil] = INIT_ACT_ASSIMIL;
  registers[actCloseAssimil] = INIT_ACT_CLOSE_ASSIMIL;
  registers[actSmallElevation] = INIT_ACT_SMALL_ELEVATION;
  registers[actLiberate] = INIT_ACT_LIBERATE;
 // register[actDoorBall] = 
  //here are the register values
  registers[qrdsIr] = INIT_QRDSIR; //this stores the value of the QRDs (bits 1 = detected)
  registers[vcnlH] = INIT_VCNL;
  registers[vcnlL] = INIT_VCNL;
  
  //servos
  registers[sldCmd] = INIT_SLD_CMD;
  registers[srdCmd] = INIT_SRD_CMD;
  
  registers[slsCmd] = INIT_SLS_CMD;
  registers[susCmd] = INIT_SUS_CMD;
  registers[sbCmd] = INIT_SB_CMD;
  registers[scamCmd] = INIT_SCAM_CMD;
  
  //Motors
  //Devices, motors,
  registers[motU] = INIT_MOTU;
  registers[motL] = INIT_MOTL;
 


  //shared with channels
  registers[sldCmdSelective] = INIT_SLDCMDSELECTIVE;
  registers[srdCmdSelective] = INIT_SRDCMDSELECTIVE;

  //shafts 
  registers[susCmdMax] = INIT_SUSCMDMAX;
  registers[slsCmdMax] = INIT_SLSCMDMAX;

  registers[sldCmdMax] = INIT_SLDCMDMAX;
  registers[srdCmdMax] = INIT_SRDCMDMAX;

  //modified
  //servoshat command

  //init channels
  registers[chanSsCmd] = INIT_CHAN_SHAFT;
  registers[chanDoorCmd] = nothing; //no command
  registers[chaninhibVision] = INIT_CHAN_INHIB_VISION;
  registers[chanColor] = INIT_CHAN_COLOR;
  registers[chanTimGame] = INIT_CHAN_TIMEGAME; 
  
  
  
}



//=================LOW_LEVEL_FUNCTIONS===================

void otisInitPorts()
{
  //Push button
  pinMode(PIN_PB ,INPUT_PULLUP);
  pinMode(PIN_QRD0,INPUT);
  pinMode(PIN_QRD1,INPUT);
  pinMode(PIN_QRD2,INPUT);
  pinMode(PIN_QRD3,INPUT);
  
  //Servos
  serv_sld.attach(PIN_SLD);
  serv_srd.attach(PIN_SRD);
  serv_slf.attach(PIN_SLF);
  serv_suf.attach(PIN_SUF);
  serv_sb.attach(PIN_SB);      
  serv_cam.attach(PIN_SCAM);
  
  //Motors
  analogWriteFrequency(PIN_MOTU, 1000);  //Frequence de xkHz, cette pin utilise le timer 0
  analogWriteFrequency(PIN_MOTL, 1000);  //Frequence de xkHz, cette pin utilise le timer 0
  
}


//Open or close the trapdoor
void ballTrapDoor(bool opened)
{
  if(opened)
  {
    serv_sb.write(BALL_TRAPDOOR_SERVO_MAX);
  }
  else
  {
    serv_sb.write(BALL_TRAPDOOR_SERVO_MIN);
  }
  
}


//This function is used to control the front door
//apperture : from 0 to 255 
//mode 0 : to contol both half doors, 0 = close and 255 = wide open
//mode 1 : left door only, 0 = close and 255 = wide open
//mode 2 : right door only, 0 = close and 255 = wide open
void otisDoor(unsigned char apperture, unsigned char mode)
{  
    unsigned char leftAngle = 255 - apperture;;
    unsigned char rightAngle;
    
    leftAngle = ((unsigned int)leftAngle*(DOOR_SERVO_MAX-DOOR_SERVO_MIN)/255) + DOOR_SERVO_MIN;
    
    rightAngle = 180 - leftAngle;  //This is the reverse
   
    //leftAngle = leftAngle - DOOR_SERVO_MIN; //(180-DOOR_SERVO_MAX);

    switch(mode)
    {
      case 0:
        serv_sld.write(leftAngle);
        serv_srd.write(rightAngle);
      break;
      case 1:
        serv_sld.write(leftAngle);   
      break;       
      case 2: 
        serv_srd.write(rightAngle);    
      break;

    }
}

//Power on or off the lower winch
void otisLowerWinch(bool on)
{
  if(on)
  {
    analogWrite(PIN_MOTL,WINCHSPEED);
  }
  else
  {
    analogWrite(PIN_MOTL,0);
  }
}

//Power on or off the upper winch
void otisUpperWinch(bool on)
{
  if(on)
  {
    analogWrite(PIN_MOTU,WINCHSPEED);
  }
  else
  {
    analogWrite(PIN_MOTU,0);
  }
}

//Control the distance of the lower Floor
//distance : From 0 (closed) to 255 (opened).
void otisLShaftDist(unsigned char distance)  
{
  unsigned char angle;
  angle = (unsigned int)((255-distance) * (L_SHAFT_DIST_SERVO_MAX-L_SHAFT_DIST_SERVO_MIN))/255
                                                              + L_SHAFT_DIST_SERVO_MIN;
  serv_slf.write(angle);  
  
}

//Control the distance of the lower Shaft
//distance : From 0 (closed) to 255 (opened).
void otisUShaftDist(unsigned char distance)
{
  unsigned char angle;
  angle = (unsigned int)(distance * (U_SHAFT_DIST_SERVO_MAX-U_SHAFT_DIST_SERVO_MIN))/255
                                                              + U_SHAFT_DIST_SERVO_MIN;
  serv_suf.write(angle); 
}


//return an register where bits 0,1,2,3 
//are showing the current state of its corresponding QRDSensor: |x|x|x|x|3|2|1|0
unsigned char otisReadProximQRD()
{
  static unsigned char buf = 0;
  
  if(digitalRead(PIN_QRD0) == HIGH)
    buf |= 0x01;
  else
    buf&= ~(0x01);
    
  if(digitalRead(PIN_QRD1) == HIGH)
    buf |= (0x01<<1);
  else
    buf&= ~(0x01<<1);

  if(digitalRead(PIN_QRD1) == HIGH)
    buf |= (0x01<<2);
  else
    buf&= ~(0x01<<2);
  
  if(digitalRead(PIN_QRD1) == HIGH)
    buf |= (0x01<<3);
  else
    buf&= ~(0x01<<3);

  return buf;
  
}





//=================MID_LEVEL_FUNCTIONS===================

//Open the door like specified in doorEnum. Doesn't do anything when doorEnum is nothing.
void otisOpenDoor(volatile unsigned char *registers, unsigned char doorEnum)
{
  //

  #define OPENED 145
  #define CLOSED 0
  #define MID 127

  #define GET_SPOT 26

  
  switch(doorEnum)
  {
    case lROpen:
      registers[sldCmd] = OPENED;
      registers[srdCmd] = OPENED;
    break;
    case lRClose:
      registers[sldCmd] = CLOSED;
      registers[srdCmd] = CLOSED;
    break;
    case lRMid:
      registers[sldCmd] = MID;
      registers[srdCmd] = MID;
    break;
    case lOpen:
      registers[sldCmd] = OPENED;
      
    break;
    case lMid:
      registers[sldCmd] = MID;
      
    break;
    case lClose:
      registers[sldCmd] = CLOSED;
      
    break;
    case rOpen:
      registers[sldCmd] = OPENED;
      registers[srdCmd] = OPENED;
    break;
    case rMid:
      
      registers[srdCmd] = OPENED;
    break;
    case rClose:
    
      registers[srdCmd] = CLOSED;
    break;
    case lCloseROpen:
      registers[sldCmd] = CLOSED;
      registers[srdCmd] = OPENED;
    break;
    case lOpenRClose:
      registers[sldCmd] = OPENED;
      registers[srdCmd] = CLOSED;
    break;    
    case lRGetSpot :
      registers[sldCmd] = GET_SPOT;
      registers[srdCmd] = GET_SPOT;
    break;
  }
}

//ReadSensors, returs the current value
unsigned char readQrds()
{
   unsigned char q = 0;
   
   if(digitalRead(PIN_QRD0))
   {
     q |= 0x01;
   }
   if(digitalRead(PIN_QRD1))
   {
     q |= 0x02;
   }
   if(digitalRead(PIN_QRD2))
   {
     q |= 0x04;
   }
   if(digitalRead(PIN_QRD3))
   {
     q |= 0x08;
   }
   return q;
}

//=================HIGH_LEVEL_FUNCTIONS===================

//This function is going to init the mecanism
void otisInit(volatile unsigned char *reg)
{
  otisInitPorts();
  vcnlInit();  //Init I2C for vcnl and the sensor itself.
  otisInitRegisters(reg);  //Init Registers used by the mecanisme
  
}


void otisReadSensors()
{
  unsigned int buf = 0;


  //If automatic_mode_Deactivated (from canal), the vncl is blind,

  if(otisReg[chaninhibVision] == CHAN_DEF_VISION_BLIND)
  {
    otisReg[vcnlH] = INIT_VCNL;
    otisReg[vcnlL] = INIT_VCNL; //VNCL is blind 
  }
  else if(otisReg[chaninhibVision] == CHAN_DEF_VISION_SEE)
  {
      otisReg[vcnlH] = 0xFF;
      otisReg[vcnlL] = 0xFF; //VNCL sees
  }
  else  //when clear, the robot can see
  { 
    buf = readProximity();  //Read the vcnl
    otisReg[vcnlH] = (buf & 0xFF00) >> 8;
    otisReg[vcnlL] = buf & 0xFF;  //get the values hihihihahahaha!
  }

  otisReg[qrdsIr] = readQrds();  //Update QRDsIR
}


//Refresh the valuesCHAN_DEF_SHAFT_CMD_FALL_SPOT
void otisRefreshDevices()
{
  static uint8_t old_time = 255;
  
  //If the game is running (or time expired)
  if(otisReg[chanTimGame] != CHAN_DEF_TIM_GAME_RESET)
  {
    
    //If new start
    if(old_time == 255)
    {
      newExec = true;
      otisReg[slsCmd] = SHAFT_CLOSED;  //Low shaft closed
      otisReg[susCmd] = SHAFT_UPPER_SPOT;  //High shaft to spots width
    }

    //Control the limitations (limited using canal)
    //=====For shafts=====
    if(otisReg[susCmd] > otisReg[susCmdMax])
    {
      otisReg[susCmd] = otisReg[susCmdMax];
    }
    if(otisReg[slsCmd] > otisReg[slsCmdMax])
    {
      otisReg[slsCmd] = otisReg[slsCmdMax];
    }
    
    //=====For the doors=====
    if(otisReg[sldCmdSelective])  //if the selective cmd
    {
      otisReg[sldCmdSelective] = otisReg[sldCmdSelective];
    }
    else if(otisReg[sldCmd] > otisReg[sldCmdMax])  //else if additive
    {
      otisReg[sldCmd] = otisReg[sldCmdMax];  
    }
    
    if(otisReg[srdCmdSelective])  //if the selective cmd
    {
      otisReg[srdCmdSelective] = otisReg[srdCmdSelective];  //Get the selective
    }
    else if(otisReg[srdCmd] > otisReg[srdCmdMax])  //else if additive
    {
      otisReg[srdCmd] = otisReg[srdCmdMax];  //Control the limit
    }  
    

    


    //If the shaft has to be controled by the channel
    if(otisReg[chanSsCmd] != CHAN_DEF_SHAFT_CMD_CLEAR)
    {
      //use the chanel as consigne for both of the 
      //@todo : protect the minimum value, (if a spot is inside the shaft)
      otisReg[susCmd] = otisReg[chanSsCmd];
      otisReg[slsCmd] = otisReg[chanSsCmd];

      if(otisReg[chanSsCmd] >= CHAN_DEF_SHAFT_CMD_FALL_SPOT)  //If this is a falling spot command
      {
          otisReg[nSpot] = INIT_NSPOT;  //reinit score, cause no more spots
      }
    } 

    if(otisReg[chanDoorCmd] != nothing) //if the channels tells that the door has to have a specific state,
    {
      otisOpenDoor(otisReg, otisReg[chanDoorCmd]);  //give the doors a new state
    }

    //#define TIME_BEFORE_OPEN_DOORS

    //maybe this is not needed, wait some times before opening the doors
    
    //  otisOpenDoor(otisReg, otisReg[chanDoorCmd]);  //give the doors a new state
  
    

  }
  else  //game time is reset
  {
    //get 
    uint8_t up,dn;

    // up = otisReg[motU]; //save
    // dn = otisReg[motL];

    otisInitRegisters(otisReg);  //Reinit Registers to originalvalues
    // otisReg[motU] = up; 
    // otisReg[motL] = dn; //apply

  }


  // }

  
  //Motors,
  otisUpperWinch((bool)otisReg[motU]);
  otisLowerWinch((bool)otisReg[motL]);
 
  //Doors

  otisDoor(otisReg[sldCmd], 1);
  otisDoor(otisReg[srdCmd], 2);
  //Door for the ball
  ballTrapDoor((bool)otisReg[sbCmd]);
  //Distance Floor (shafts)
  otisLShaftDist(otisReg[slsCmd]);
  otisUShaftDist(otisReg[susCmd]);


  old_time = otisReg[chanTimGame];  //refresh
 
}

/**
 * This is called once at start in order to verify the behaviors.
 */
void checkOtisStart()
{
  #define SHAFT_OPENED 255
  #define SHAFT_CLOSED_DEMO 16

  otisReg[slsCmd] = SHAFT_OPENED; //lower shaft
  otisReg[susCmd] =  SHAFT_OPENED; //upper shaft

  
  otisReg[motL] = 1;
  delay(1000);
  otisReg[motU] = 1;
  delay(1000);
  otisReg[slsCmd] = SHAFT_CLOSED_DEMO; //shafts
  otisReg[susCmd] =  SHAFT_CLOSED_DEMO;
  

  otisOpenDoor(otisReg, lROpen);
  

  otisReg[motL] = 0;
  otisReg[motU] = 0;
  delay(1000);

}

//This function is intended to be called by the main
void runOtisRun()  
{
  //===DEFINES==============
  //===Rules && mechanics=== 
  


  //TIME AND DELAY
  #define TIMEOUT_CAPTURE 800    //800 [ms]
  #define DELAY_OPENUPSHAFT 200
  
  


  //SHAFT_SENSORS
  // #define QRD0 (otisReg[qrdsIr]&0x01)  //old values before new wiring
  // #define QRD1 ((otisReg[qrdsIr]&0x02)>>1)
  // #define QRD2 ((otisReg[qrdsIr]&0x04)>>2)
  // #define QRD3 ((otisReg[qrdsIr]&0x08)>>3)

  //new values because the wiring changed.
  #define QRD3 (otisReg[qrdsIr]&0x01)
  #define QRD1 ((otisReg[qrdsIr]&0x02)>>1)
  #define QRD2 ((otisReg[qrdsIr]&0x04)>>2)
  #define QRD0 ((otisReg[qrdsIr]&0x08)>>3)
  


  //#define VISION_THRESHOLD_CAPTURED 20000
  //========================
  //local variables
  static unsigned char lastCatchAttempt = FALSE;
  static uint8_t oldQRD0 = 0;
  
  bool visionDetectObject = false;
  unsigned int timeout;
  unsigned int visionVcnl;
  //===============
  
  

  //open doors
  

  if(newExec)
  {
    //otisOpenDoor(otisReg, lRClose); //todo
    otisOpenDoor(otisReg, lROpen);
    otisReg[slsCmd] = SHAFT_OPENED;  //Low shaft opened
    otisReg[susCmd] = SHAFT_UPPER_SPOT;  //High shaft to spots width
    newExec = false;
  }
  

  
  visionVcnl = 0;
  //Get last vision, this is in critical section
  noInterrupts(); 
    visionVcnl |=  otisReg[vcnlL];
    visionVcnl |= otisReg[vcnlH]<<8;
  interrupts(); 
  
  //according to the color, check 
  if(otisReg[chanColor] == JAUNE)
  {
    if(visionVcnl >= VISION_THRESHOLD_SEE_JAUNE)
    {
      visionDetectObject = true;
    }
  }
  else  //IF vert
  {
    if(visionVcnl >= VISION_THRESHOLD_SEE_VERT)
    {
      visionDetectObject = true;
    }

  }


  //If something like object is detected (and the system is not inhibated by interrupt) and doors not closed
  if((visionDetectObject == true) && (otisReg[sldCmd] != 0 )) //added
  {
    //Close de doors
    otisOpenDoor(otisReg, lRGetSpot);
    
    timeout = millis();
     
    //Try to see if it has captured it, or timeout (TIMEOUT_CAPTURE)
    while((QRD0 != 1) && ((millis() - timeout) < TIMEOUT_CAPTURE));
    
    
 
    
    //If success,
    if(QRD0)
    {
      
      //Close the lower shaft
      otisReg[slsCmd] = SHAFT_LOWER_SPOT;  //Lower shaft spot width
      delay(200); //wait on the mechanics before the information is shared.
      
      if(oldQRD0 == 0)  //If this is really a new one
      {
        otisReg[nSpot]++;  //new spot inside
      }
      //open and close upper shaft
      //otisReg[susCmd] = SHAFT_UPPER_SPOT+20;      //upper OPENED
      //delay(DELAY_OPENUPSHAFT);
      //otisReg[susCmd] = SHAFT_LOWER_SPOT;  //upper spot wodth
    
      
      //Time to make the winches run 
      otisReg[motL] = 1;
      delay(1200);
      otisReg[motU] = 1;
   
    //  while(QRD0);  //wait for something in the mouth
      
      
      while(QRD1 == 0);  //wait for QRD1 to see the spot
      //open doors
      otisOpenDoor(otisReg, lROpen);
              //while the spot is not in the upper (stockage) shaft
      while(QRD2 == 0);
        
        //And has passed the turning point between both of the 
      while(QRD1);
        
        /*
      //while(otisReg[qrdsIr] != 0x02);
      if(QRD2)  //If a spot is already in the system
      {
        
        while(
      }
      else
      {
       
        //while the spot is not in the upper (stockage) shaft
        while(QRD2 == 0);
        
        //And has passed the turning point between both of the 
        while(QRD1);
      } */
      //wait for disparition in the mouth
     
      
      otisReg[motL] = 0;
      otisReg[motU] = 0;
      
      otisReg[slsCmd] = SHAFT_OPENED;  //Open the lower shaft, get ready to fuck big momaaa!
      

      lastCatchAttempt = TRUE;
      
      
          
    }
    else
    {
      lastCatchAttempt = FALSE;
      otisOpenDoor(otisReg, lROpen); 
    }
    
    
    //not needed anymore
    // if(lastCatchAttempt)  //A new spots is in the system,
    // {
    //   //Add one to the number of spots in da system
    //   //otisReg[nSpot]++;                   //commented on 18 may 2015
    //   lastCatchAttempt = FALSE;
    // }
    
  }
  else  //if nothing is seen by vncl
  {
    if((QRD0)&& (oldQRD0 == 0))  //but a spot is detected in the mouth
    {
      //Primary reflex : 
      //Close the lower shaft
      otisReg[slsCmd] = SHAFT_LOWER_SPOT;  //Lower shaft spot width
      delay(200); //wait on the mechanics before the information is shared.
      otisReg[nSpot]++;  //new spot inside
    }

  }
  oldQRD0 = QRD0; //maj
  
}





void otisI2CInitChannels()
{
  
  // //Publishing
  // WirePubSub::addChannel(SPOTS_NB_BALLES,'w',NULL);  //Add publishing channel
  // //Subscribing
  // WirePubSub::addChannel(RPI_SPOT_INHIB_VISION,'r',NULL);  //Add each listening

  // WirePubSub::addChannel(RPI_SPOT_PORTES,'r',NULL);  //Add each listening     @todo
  // WirePubSub::addChannel(RPI_SPOT_SHAFT,'r',NULL);  //Add each listening
  // WirePubSub::addChannel(MASTER_COLOR,'r',NULL);  //Add each listening
  // WirePubSub::addChannel(MASTER_TIMER,'r',NULL);  //Add each listening

  Serial.begin(115200);

}

/**
 * @brief this 
 * @details [long description]
 */
void otisI2CPublish()
{
  static uint8_t oldnSpot = (INIT_NSPOT+1);  //different than the init value

  if(otisReg[nSpot] != oldnSpot)  //if new value, detected,
  {
    //noInterrupts();
    //WirePubSub::write(SPOTS_NB_BALLES,otisReg[nSpot]);  //(publish) write the number of balls
    json_send["nb_spots"] = otisReg[nSpot];
    json_send.printTo(Serial);
    //interrupts();
  }

  oldnSpot = otisReg[nSpot];  //maj
}
  
 
void otisI2CReadSubs()
{
  //reading the new values

  uint8_t portesTmp, shaftTmp;

  if (stringComplete) {
    json_rcv = jsonBuffer.parseObject(json);
    // clear the string:
    inputString = "";
    stringComplete = false;
  }

  if(root.success())
  {
    //WirePubSub::read(RPI_SPOT_INHIB_VISION, (uint8_t*)&otisReg[chaninhibVision]);
    if(json_rcv.containsKey("spot_inhib_vision"))
    {
      otisReg[chaninhibVision] = json_rcv["spot_inhib_vision"];
    }


    // if(WirePubSub::read(RPI_SPOT_PORTES, &portesTmp) == MODIFIED)   //ADDED 11h36 21 may 2015
    // {
    //   otisReg[chanDoorCmd] = portesTmp;
    // }
    if(json_rcv.containsKey("spot_door"))
    {
      otisReg[chanDoorCmd] = json_rcv["spot_door"];
    }
    // if(WirePubSub::read(RPI_SPOT_SHAFT, &shaftTmp) == MODIFIED)
    // {
    //   otisReg[chanSsCmd] = shaftTmp;
    // }
    if(json_rcv.containsKey("spot_shaft"))
    {
      otisReg[chanSsCmd] = json_rcv["spot_shaft"];
    }

    // WirePubSub::read(MASTER_COLOR, (uint8_t*)&otisReg[chanColor]);
    if(json_rcv.containsKey("team_color"))
    {
      otisReg[chanColor] = json_rcv["team_color"];
    }
    // WirePubSub::read(MASTER_TIMER, (uint8_t*)&otisReg[chanTimGame]);
    if(json_rcv.containsKey("start_timer"))
    {
      otisReg[chanTimGame] = json_rcv["start_timer"];
    }

  }

}

//== Serial ==
/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
}
  
