#include <i2c_t3.h>
#include "Arduino.h"
#include "i2cSlave.h"
#include "global.h"  //include global variables


void setupI2cSlave()
{
  // Setup for Slave mode, address I2CADD, pins 18/19, external pullups, 100kHz
  Wire.begin(I2C_SLAVE, I2CADD, I2C_PINS_18_19, I2C_PULLUP_EXT, I2C_RATE_100);
  Wire.onReceive(i2cSlaveReceiveEvent);
  Wire.onRequest(i2cSlaveRequestEvent);
}


//
// handle Rx Event (incoming I2C request/data)
//
void i2cSlaveReceiveEvent(size_t len)
{
  //digitalRead(LED_BUILTIN)^1);
  if (Wire.available())
  {
  
    /*
            // grab command
            cmd = Wire.readByte();

            switch(cmd)
            {
            //DANS LE CAS D'UNE COMMANDE D'ECRITURE AU MODULE
            case WRITE:

              data_rec = Wire.readByte(); //lecture de la donnée envoyée au module
              */

    /*
        addr = Wire.readByte();                // grab addr

        while(Wire.available())  //Tant que donnée reçue,
            if(addr < MEM_LEN)                 // drop data beyond mem boundary
                mem[addr++] = Wire.readByte(); // copy data to mem
            else
                Wire.readByte();               // drop data if mem full
        break;
    */

    /*
    case READ:
        addr = Wire.readByte();                // grab addr
        break;

    case SETRATE:
        rate = (i2c_rate)Wire.readByte();                // grab rate
        Wire.setRate(F_BUS, rate);             // set rate
        */
  }
}



//
// handle Tx Event (outgoing I2C data)
//
void i2cSlaveRequestEvent(void)
{ /*
     switch(cmd)
     {
     case READ:
         Wire.write(&mem[addr], MEM_LEN-addr); // fill Tx buffer (from addr location to end of mem)
         break;
     }*/
}



