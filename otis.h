#ifndef OTIS_H
#define OTIS_H

//Size of the array of the registers
#define OTIS_RSIZE 34

//PINS
#define PIN_SLD 23  //Servo left door
#define PIN_SRD 22  //Servo right door
#define PIN_SLF 21  //Servo lower floor
#define PIN_SUF 20  //Servo upper floor
#define PIN_SB 9    //Servo ball
#define PIN_SCAM  10  //Servo camera

#define PIN_MOTU 5  //Upper motor
#define PIN_MOTL 6  //Lower motor
//BP
#define PIN_PB 2  //Push

//Proximity detectors
#define PIN_QRD0 12//7
#define PIN_QRD1 11//8
#define PIN_QRD2 8//11
#define PIN_QRD3 7//12




//Global enum
enum ternary {
  
  flagIdle,
  flagAck,
  nSpot,
  flagCatchAttempt,
  flagLastCatch,
  actDoor,
  actCloseDoor,
  actAssimil,
  actCloseAssimil,
  actSmallElevation,
  actLiberate,
  actDoorBall,
  
  qrdsIr,
  vcnlH,                          //This is shared with canal
  vcnlL,                          //This is shared with canal
  
  sldCmd,  //from 0 to 255        
  srdCmd,  //from 0 to 255        
  
  susCmd,  //from 0 to 255                        
  slsCmd,  //from 0 to 255
  sbCmd,   //from 0 to 1
  scamCmd,  //from 0 to 1
  motU,    //this is on or off (0 or 1)
  motL,    //this is on or off (0 or 1)
  
  
  //From here it's going to be the shared with canal only


  //begin, the code below is not used in reality, only implemented.
  sldCmdSelective,  //This is inactive when 0 otherwise, it takes the control over sldCmd
  srdCmdSelective,  //This is inactive when 0 otherwise, it takes the control over srdCmd
  susCmdMax,
  slsCmdMax,
  sldCmdMax,
  srdCmdMax,
  //end of testing behavior


  //shafts selective command
  chanSsCmd,    //added 19 may
  chanDoorCmd,  //
  chaninhibVision,   //modified 19 may
  chanColor,
  chanTimGame
  
  
};


//Enum for the front door
enum fDoor
{
  nothing,  //doesn't change anything
  lROpen,
  lRMid,
  lRClose,
  lOpen,
  lMid,
  lClose,
  rOpen,
  rMid,
  rClose,
  lCloseROpen,
  lOpenRClose,
  lRGetSpot
};


//====Global variable======
extern volatile unsigned char otisReg[OTIS_RSIZE];  //extern because need to be visible from everywhere
//=========================


//Function prototypes
void otisInit(volatile unsigned char *reg);
void otisReadSensors();
void otisRefreshDevices();

/**
 * This is called once at start in order to verify the behaviors.
 */
void checkOtisStart();
void runOtisRun();



//for I2C communication
void otisI2CInitChannels();
void otisI2CPublish();
void otisI2CReadSubs();
  

#endif
