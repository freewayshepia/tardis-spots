#ifndef I2CSLAVE_H
#define I2CSLAVE_H

//#include "Arduino.h"

#define I2CADD 0x0A


void i2cSlaveReceiveEvent(size_t len);
void i2cSlaveRequestEvent(void);
void setupI2cSlave();

#endif
