#include <TimerOne.h>
#include <TimerThree.h>
#include <i2c_t3.h>
#include <WirePubSubTeensy.h>

#include <Servo.h>
#include "otis.h"
//#include "vcnl.h"
#define LED 13
//#define I2CADDRESS 0x50  //I2C address for wirepubsub is 0x50 


//Pour calibrer le periph:
//mettre val 127 dans les registres suivants : tisReg[slsCmd] et tisReg[susCmd].
//Avec ces vals, les barres de rotation des servos devront-^tre à la verticale.

//Plus la valeur de otisReg[slsCmd] est grande, plus la bouche est ouverte!
//Plus la valeur de otisReg[susCmd] est grande, plus la bouche est ouverte!


//Function prototypes
//void runBaby();
//void updateDevice();
//=====================

#define TIM1_MICRO_S  10000 //every 10 ms //Take care, it's a 16 bits timer!




//unsigned int c = 0;




void interruptTimer3()
{
  static unsigned int cnt3 = 0;

  if (cnt3 >= 10)
  {
    printOtisRegisters();
    cnt3 = 0;
  }
  else
    cnt3++;

}



void printOtisRegisters()
{
  unsigned int vcnl = 0;

  //Serial.println("");
  //Serial.println("-----Sensors-----");

  //================================Print sensors registers
  //Serial.print("QRD");
  //Serial.print("\t0b");
  //Serial.println(otisReg[qrdsIr], BIN);


#define QRD3 (otisReg[qrdsIr]&0x01)
#define QRD1 ((otisReg[qrdsIr]&0x02)>>1)
#define QRD2 ((otisReg[qrdsIr]&0x04)>>2)
#define QRD0 ((otisReg[qrdsIr]&0x08)>>3)

  //Serial.print("QRD0");
  //Serial.print("\t0b");
  //Serial.println(QRD0);
  //Serial.print("QRD1");
  //Serial.print("\t0b");
  //Serial.println(QRD1);
  //Serial.print("QRD2");
  //Serial.print("\t0b");
  //Serial.println(QRD2);
  //Serial.print("QRD3");
  //Serial.print("\t0b");
  //Serial.println(QRD3);

  vcnl |=  otisReg[vcnlL];
  vcnl |= otisReg[vcnlH] << 8;
  //Serial.print("VCNL");
  //Serial.print("\t");
  //Serial.println(vcnl, DEC);

  //=================================Print Devices registers
  //Serial.println("-----Devices-----");

  //Serial.print("motU");
  //Serial.print("\t");
  //Serial.println(otisReg[motU], DEC);

  //Serial.print("motL");
  //Serial.print("\t");
  //Serial.println(otisReg[motL], DEC);

  //Serial.print("sbCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[sbCmd], DEC);

  //Serial.print("scamCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[scamCmd], DEC);

  //Serial.print("sufCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[susCmd], DEC);

  //Serial.print("slfCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[slsCmd], DEC);

  //Serial.print("sleftDoorCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[sldCmd], DEC);

  //Serial.print("srightDoorCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[srdCmd], DEC);

  //Rules


  //Serial.print("nspots");
  //Serial.print("\t");
  //Serial.println(otisReg[nSpot], DEC);

  //Serial.println("Channels");

  //Serial.print("inhibvision");
  //Serial.print("\t");
  //Serial.println(otisReg[chaninhibVision], DEC);



  //Serial.print("doorsCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[chanDoorCmd], DEC);

    //Serial.print("shaftCmd");
  //Serial.print("\t");
  //Serial.println(otisReg[chanSsCmd], DEC);



  //Serial.print("color");
  //Serial.print("\t");
  //Serial.println(otisReg[chanColor], DEC);

  //Serial.print("timGame");
  //Serial.print("\t");
  //Serial.println(otisReg[chanTimGame], DEC);


}





void setup() {


  pinMode(LED, OUTPUT);

  //WAIT_
  //delay(1000);

  otisInit(otisReg);  //Setup otis internal's registers.
  //vcnlInit();


  //WirePubSub::begin(I2CADDRESS);  //The i2c (pins 18 and 19) is configured as slave usign pubsub system @todo
  otisI2CInitChannels();  //init the channels   @todo: decomment

  //Serial.begin(115200);  // //Serial's used to debug and print data
  Timer1.initialize(TIM1_MICRO_S);
  Timer1.attachInterrupt(refreshSystem);

  //this is made every 20 ms
  Timer3.initialize(20450);
  Timer3.attachInterrupt(interruptTimer3);

  // noInterrupts();  //Disable interrupts

  // interrupts();  //Enable it

  // otisReg[slsCmd] = 0;
  //otisReg[susCmd] = 0;


  //Let's do a simple demo before starting
  checkOtisStart();

}


void loop() {

  // put your main code here, to run repeatedly:
  //if()
  // runOtisRun();  //run the machine state of otis

  //
  // print register states over serial protocol




  runOtisRun();
  /*
  otisReg[motU] = 0;
  otisReg[motL] = 0;
  otisReg[slsCmd] = 110;
  otisReg[susCmd] = 110;
  delay(2000);
  */

  /*
  otisReg[slfCmd] = (otisReg[slfCmd] + 10 ) %190;
  if(!(c&0x01))
  {
    //otisReg[sufCmd] = 0;
    otisReg[srdCmd]=0;
    otisReg[sldCmd]=255;
  }
  els
  {
    //otisReg[sufCmd] = 0xFF;
    otisReg[srdCmd]=255;
    otisReg[sldCmd]=0;

  }

  otisReg[motL] ^= 1;

  otisReg[motU] ^= 1;
  printOtisRegisters();
  delay(2000);  // You may want to uncomment this for visibility
  c++;*/
}


void showUcAlive()
{
#define CNTMAXVAL 100
  static int cnt = 0;
  //Make the led pop
  if (cnt >= CNTMAXVAL)
  {
    if (digitalRead(LED))
    {
      digitalWrite(LED, LOW);
    }
    else
    {
      digitalWrite(LED, HIGH);
    }
    cnt = 0;
  }
  else
  {
    cnt++;
  }
}
//Here is the code to generate the interruption process

//used once to read states from internal register and write to peripherals ====================
void refreshSystem()
{
  static int toggle = 0;
  //static int i2cnt = 0;

  showUcAlive();


  //Read your sensors biatch!
  //digitalWrite(LED,HIGH);

  if (!(toggle & 0x01)) //when pair
  {
    //digitalWrite(LED,HIGH);
    otisReadSensors();
    otisI2CPublish();   //publish the latest datas to I2C @todo

    //i2cnt++;  //Every new cycle,

    //digitalWrite(LED,LOW);
  }
  else  //when impair, give the value to the devices like servomotors...
  {
    //digitalWrite(LED,HIGH);

    otisI2CReadSubs();      //Get new values from I2C
    otisRefreshDevices();   //Refresh devices according to the internal registers (sensors...) and the I2C


    //digitalWrite(LED,LOW);
  }


  //digitalWrite(LED,LOW);


  //Get I2C info's

  toggle++;

}


